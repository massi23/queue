package ch.gmtech.squeue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class QueueTest {

	@Test
	public void testAddAndRetrieve() {
		SimpleQueue<String> queue = new SimpleQueue<String>();
		queue.enqueue("massi");
		assertEquals("massi", queue.dequeue());
	}
	
	@Test
	public void 	tesAddingManyElements() {
		SimpleQueue<String> queue = new SimpleQueue<String>();
		queue.enqueue("massi");
		queue.enqueue("matteo");
		
		assertEquals("massi", queue.dequeue());
		assertEquals("matteo", queue.dequeue());
	}

}
