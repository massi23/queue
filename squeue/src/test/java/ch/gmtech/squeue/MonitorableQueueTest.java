package ch.gmtech.squeue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MonitorableQueueTest {

	@Test
	public void testEmpty() {
		MonitorableQueue<String> queue = new MonitorableQueue<String>(new SimpleQueue<String>());
		assertEquals(0, queue.maxSize());
	}
	
	@Test
	public void testSize() {
		MonitorableQueue<String> queue = new MonitorableQueue<String>(new SimpleQueue<String>());
		
		queue.enqueue("massi");
		assertEquals(1, queue.maxSize());
		
		queue.enqueue("teo");
		assertEquals(2, queue.maxSize());

		queue.dequeue();
		assertEquals(2, queue.maxSize());

		queue.enqueue("massi");
		assertEquals(2, queue.maxSize());
	}

}
