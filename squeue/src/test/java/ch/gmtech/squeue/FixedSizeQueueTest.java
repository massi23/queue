package ch.gmtech.squeue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FixedSizeQueueTest {

	@Test
	public void test() {
		FixedSizeQueue<String> queue = new FixedSizeQueue<String>(2, "unknown");
		queue.enqueue("massi");
		queue.enqueue("teo");
		queue.enqueue("gabri");
		assertEquals("massi", queue.dequeue());
		assertEquals("teo", queue.dequeue());
		assertEquals("unknown", queue.dequeue());
	}

}
