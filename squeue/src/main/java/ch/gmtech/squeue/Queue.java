package ch.gmtech.squeue;

public interface Queue<T> {

	public void enqueue(T aValue);
	public T dequeue();

}
