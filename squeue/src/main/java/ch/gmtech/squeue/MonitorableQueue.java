package ch.gmtech.squeue;

public class MonitorableQueue<T> implements Queue<T> {

	private int _maxSize;
	private int _currentSize;
	private final Queue<T> _queue;

	public MonitorableQueue(Queue<T> queue) {
		_maxSize = 0;
		_currentSize = 0;
		_queue = queue;
	}
	
	public void enqueue(T aValue) {
		_currentSize++;
		if((_currentSize) > _maxSize) _maxSize++;
		_queue.enqueue(aValue);
	}

	public T dequeue() {
		_currentSize--;
		return _queue.dequeue();
	}

	public int maxSize() {
		return _maxSize;
	}
	
}
