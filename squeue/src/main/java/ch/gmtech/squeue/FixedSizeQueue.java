package ch.gmtech.squeue;

public class FixedSizeQueue<T> implements Queue<T> {

	private final int _maxSize;
	private int _currentSize;
	private final Queue<T> _queue;
	private final T _defaultValue;

	public FixedSizeQueue(int size, T defaultValue) {
		_currentSize = 0;
		_maxSize = size;
		_defaultValue = defaultValue;
		_queue = new SimpleQueue<T>();
	}
	
	
	public void enqueue(T aValue) {
		if(_currentSize >= _maxSize) return;
		_currentSize++;
		_queue.enqueue(aValue);
	}

	public T dequeue() {
		if(_currentSize == 0) return _defaultValue;
		T result = _queue.dequeue();
		_currentSize --;
		return result;
	}

}
