package ch.gmtech.squeue;

import java.util.ArrayList;
import java.util.List;

public class SimpleQueue<T> implements Queue<T> {

	private final List<T> _storage;

	public SimpleQueue() {
		_storage = new ArrayList<T>();
	}

	public void enqueue(T aValue) {
		_storage.add(aValue);
	}

	public T dequeue() {
		return _storage.remove(0);
	}

}
